
**checkJobCalendar**

	日付によるバッチの実行制御

---

[TOC]

---

## Usage

	日付による後続処理の判定が必要な箇所(command)で、
	日付を列挙したテキストファイルをvbsの引数に指定して起動します。
	日付が一致した場合、実行結果(%ERRORLEVEL%)に 0 が返却されます。

	ex)
	cscript  //nologo  checkJobCalendar.vbs  calendarList.log
	IF "%ERRORLEVEL%" == "0" (
	  echo Applicable
	)

	日付を列挙するテキストファイルは YYYYMMDD形式で記載し、
	改行またはカンマで区切ってください。
	ex) calendarList.log
		20200804
		20200902


## Note

	・vbs, bat, log は日本語を含むため、ファイルのエンコードはsjisを前提としています。
	・タスクスケジューラ等の細かな日付管理をしない呼出元から起動されるバッチ処理で、起動日を限定する用途を想定しています。


---

## Author

	inishie
	inishie77@hotmail.com
	https://bitbucket.org/inishie/

## License

Copyright (c) 2022 inishie.  
Released under the [MIT license](https://en.wikipedia.org/wiki/MIT_License).
