' 日付による処理要否の判定
' (スケジューラの日次バッチによる起動・%ERRORLEVEL%判定 を想定する)
'   引数1 : YYYYMMDDのリストファイル (改行やカンマ区切り)
'   復帰値: 日付の有無 (0:該当あり, 1:該当なし, 2:設定不備)
' ex)  cscript  //nologo  checkJobCalendar.vbs  calendarList.log

Option Explicit

Dim objArgs          '引数
Dim strFormattedDate 'yyyymmdd
Dim isIncDate        '復帰値

Set objArgs = WScript.Arguments

If objArgs.Count = 0 Then
	'引数は必須
	WScript.Echo "実行日リストのファイルを指定してください"
	WScript.Quit 2
End If

'yyyymmdd 形式で現在日付を取得
strFormattedDate = Replace(Left(Now(),10), "/", "")

isIncDate = IsInclusionDate(objArgs(0), strFormattedDate)
WScript.Quit isIncDate


'--------------------------------------
'同値があれば 0 を返す
' filePath      検索先ファイル
' targetDate    対象日(yyyymmdd)
'
Function IsInclusionDate(filePath, targetDate)

	Dim text1, objFSO, objFile1
	text1 = ""

	Set objFSO = CreateObject("Scripting.FileSystemObject")
	On Error Resume Next ' 強行
		If objFSO.FileExists(filePath) Then
			Set objFile1 = objFSO.OpenTextFile(filePath, 1) 
			If Err.Number = 0 Then
				text1 = objFile1.ReadAll 
			End If
			objFile1.Close
		End If
	On Error Goto 0 ' 解除

	Set objFSO = Nothing

	IsInclusionDate = 1
	If text1 = "" Then
		' 指定日無し
		IsInclusionDate = 1
	ElseIf InStr(text1, targetDate) > 0 Then 
		' 指定日が含まれる
		IsInclusionDate = 0
	End If 

End Function

